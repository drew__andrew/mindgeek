﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using MindGeek.DataAccess;
using MindGeek.DataAccess.Interfaces;

namespace MindGeek
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory()+ "//"+ "Web")
                .UseStartup<Startup>()
                .Build();

            var jsonLoader = (IJsonLoader) webHost.Services.GetService(typeof(IJsonLoader));
            DataSeeder.FetchData(jsonLoader);

            webHost.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
