﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MindGeek.Business.Interfaces;
using MindGeek.Business.Models;
using MindGeek.Models;

namespace MindGeek.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFirstPageService _firstPageService;
        private readonly ISecondPageService _secondPageService;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IFirstPageService FirstPageService,ISecondPageService SecondPageService, ILogger<HomeController> Logger)
        {
            _firstPageService = FirstPageService;
            _secondPageService = SecondPageService;
            _logger = Logger;
        }

        public IActionResult Index()
        {
            List<Card> cards = _firstPageService.GetCardsForFirstPage();
            return View(cards);
        }

        public IActionResult CardDetails(string id)
        {
            CardDetails cardDetails = _secondPageService.GetCardDetails(id);
            return View(cardDetails);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
