﻿using System.Collections.Generic;
using MindGeek.Business.Models;
using MindGeek.DataAccess.Interfaces;

namespace MindGeek.Common.Helpers
{
    public interface ICacheDataHolder
    {
        void TryInsertDataToDictionary(string cardId, byte[] data, MediaType mediaType);

        CardPicturesCache GetCardPicturesCache(string cardId);

        bool CheckIfMediaShouldDownload(IDtoMediaCachable cachableEntity);
    }
}