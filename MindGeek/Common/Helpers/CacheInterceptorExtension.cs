﻿using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using System.Collections.Generic;
using MindGeek.DataAccess.Models.Dto;

namespace MindGeek.Common.Helpers
{
    public static class CacheInterceptorExtension
    {
        public static ICollection<T> InterceptMediaForCaching<T>(this ICollection<T> sequence, IMediaDownloadHelper mediaDownloadHelper) where T : IBaseModel<BulkDataItem>, new()
        {
            foreach (var item in sequence)
            {
                switch (item.GetType().Name)
                {
                    case nameof(CardDto):
                        mediaDownloadHelper.TryDownloadMedia(item as CardDto);
                        break;
                    case nameof(CardKeyArtImagesDto):
                        mediaDownloadHelper.TryDownloadMedia(item as CardKeyArtImagesDto);
                        break;
                    case nameof(CardImagesDto):
                        mediaDownloadHelper.TryDownloadMedia(item as CardImagesDto);
                        break;
                    default:
                        continue;
                }
            }
            return sequence;
        }
    }
}
