﻿using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models.Dto;

namespace MindGeek.Common.Helpers
{
    public interface IMediaDownloadHelper
    {
        void TryDownloadMedia(IDtoMediaCachable cachableContent);
    }
}
