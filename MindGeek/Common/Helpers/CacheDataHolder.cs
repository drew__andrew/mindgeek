﻿using System.Collections.Generic;
using MindGeek.Business.Models;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models.Dto;

namespace MindGeek.Common.Helpers
{
    public class CacheDataHolder : ICacheDataHolder
    {
        private Dictionary<string, CardPicturesCache> cardPictures;
        private Dictionary<string, KeyArtPicturesCache> keyArtPictures;

        public CacheDataHolder()
        {
            cardPictures = new Dictionary<string, CardPicturesCache>();
            keyArtPictures = new Dictionary<string, KeyArtPicturesCache>();
        }

        public CardPicturesCache GetCardPicturesCache(string cardId)
        {
            if (cardPictures.ContainsKey(cardId))
                return cardPictures[cardId];
            return null;
        }

        public bool CheckIfMediaShouldDownload(IDtoMediaCachable cachableEntity)
        {
            switch (cachableEntity.GetType().Name)
            {
                case nameof(CardDto):
                    return !cardPictures.ContainsKey(cachableEntity.GetCardId());
                case nameof(CardKeyArtImagesDto):
                    return !keyArtPictures.ContainsKey(cachableEntity.GetCardId());
                default:
                    return true; //we don't want to download
            }
        }

        public void TryInsertDataToDictionary(string cardId, byte[] data, MediaType mediaType)
        {
            if (mediaType == MediaType.CardPicture)
            {
                if (cardPictures.ContainsKey(cardId))
                {
                    cardPictures[cardId].AddCardPictureCache(data);
                }
                else
                {
                    cardPictures.TryAdd(cardId, new CardPicturesCache(data));
                }
            }

            if (mediaType == MediaType.KeyArtImages)
            {
                if (keyArtPictures.ContainsKey(cardId))
                {
                    keyArtPictures[cardId].AddCardPictureCache(data);
                }
                else
                {
                    keyArtPictures.TryAdd(cardId, new KeyArtPicturesCache(data));
                }
            }
        }
    }
}