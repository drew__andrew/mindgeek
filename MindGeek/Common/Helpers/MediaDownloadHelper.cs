﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MindGeek.DataAccess.Interfaces;

namespace MindGeek.Common.Helpers
{
    public class MediaDownloadHelper : IMediaDownloadHelper
    {
        private ICacheDataHolder _dataHolder;
        private ILogger<MediaDownloadHelper> _logger;

        public MediaDownloadHelper(ICacheDataHolder dataHolder, ILogger<MediaDownloadHelper> logger)
        {
            _dataHolder = dataHolder;
            _logger = logger;
        }

        public void TryDownloadMedia(IDtoMediaCachable cachableContent)
        {
           bool shouldDownload = _dataHolder.CheckIfMediaShouldDownload(cachableContent);
            
            if (shouldDownload)
            {
                var downloadBlock = new List<Task<byte[]>>();
                foreach (var item in cachableContent.GetMetadataDto())
                {
                    downloadBlock.Add(Task.Run(async () => await Download(item.Url)));
                }

                Task.WaitAll(downloadBlock.ToArray());
                foreach (var task in downloadBlock)
                {
                    byte[] imageBytes = task.Result;
                    _dataHolder.TryInsertDataToDictionary(cachableContent.GetCardId(), imageBytes, cachableContent.MediaType);
                    cachableContent.Media.Add(imageBytes);
                }
            }
            else
            {
                cachableContent.Media = _dataHolder.GetCardPicturesCache(cachableContent.GetCardId()).Pictures;
            }
        }

        private async Task<byte[]> Download(string url)
        {
            using (var webclient = new WebClient())
            {
                try
                {
                    return webclient.DownloadDataTaskAsync(new Uri(url)).Result;
                }
                catch (Exception ex)
                {
                    //log it
                    return null;
                }
            }
        }

    }
}
