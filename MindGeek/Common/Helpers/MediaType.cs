﻿namespace MindGeek.Common.Helpers
{
    public enum MediaType
    {
        CardPicture,
        KeyArtImages,
        Movie
    }
}