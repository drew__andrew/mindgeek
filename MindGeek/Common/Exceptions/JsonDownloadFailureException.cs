﻿using System;

namespace MindGeek.Common.Exceptions
{
    public class JsonDownloadFailureException : Exception
    {
        public JsonDownloadFailureException(string url) : base(String.Format("Downloading Json from url: {0} failed", url))
        {
            
        }
    }
}