﻿using System.Collections.Generic;
using MindGeek.DataAccess.Models;
using MindGeek.DataAccess.Models.Dto;

namespace MindGeek.Common
{
    public static class DataConversionExtensions
    {
        public static List<MetadataDto> TransferMetadataFromDbToDtoObject(this List<CardImages> toTransfer)
        {
            List<MetadataDto> toReturn = new List<MetadataDto>();
            toTransfer.ForEach(x => toReturn.Add(new MetadataDto
            {
                H = x.H,
                W = x.W,
                Url = x.Url
            }));

            return toReturn;
        }

        public static List<MetadataDto> TransferMetadataFromDbToDtoObject(this List<KeyArtImages> toTransfer)
        {
            List<MetadataDto> toReturn = new List<MetadataDto>();
            toTransfer.ForEach(x => toReturn.Add(new MetadataDto
            {
                H = x.H,
                W = x.W,
                Url = x.Url
            }));

            return toReturn;
        }

        public static List<CastDto> TransferMetadataFromDbToDtoObject(this List<Cast> toTransfer)
        {
            List<CastDto> toReturn = new List<CastDto>();
            toTransfer.ForEach(x => toReturn.Add(new CastDto
            {
                Name = x.Name
            }));

            return toReturn;
        }

        public static List<DirectorsDto> TransferMetadataFromDbToDtoObject(this List<Directors> toTransfer)
        {
            List<DirectorsDto> toReturn = new List<DirectorsDto>();
            toTransfer.ForEach(x => toReturn.Add(new DirectorsDto
            {
                Name = x.Name
            }));

            return toReturn;
        }
    }
}