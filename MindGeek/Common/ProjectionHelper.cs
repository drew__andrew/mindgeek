﻿using MindGeek.DataAccess.Interfaces;
using System;
using System.Linq.Expressions;

namespace MindGeek.Common
{
    public static class ProjectionHelper<TEntity, TModel> where TModel : IBaseModel<TEntity>, new()
    {
        public static Expression<Func<TEntity, TModel>> GetDefaultProjection()
        {
            var model = new TModel();
            Expression<Func<TEntity, IBaseModel<TEntity>>> expression = model.GetDefaultProjection();
            return Expression.Lambda<Func<TEntity, TModel>>(expression.Body, expression.Parameters);
        }
    }
}