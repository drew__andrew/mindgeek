﻿using System.Collections.Generic;

namespace MindGeek.Business.Models
{
    public class Card 
    {
        public string Title;

        public string Id;

        public List<byte[]> Pictures = new List<byte[]>();
    }
}