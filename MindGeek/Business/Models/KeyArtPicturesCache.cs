﻿using System.Collections.Generic;

namespace MindGeek.Business.Models
{
    public class KeyArtPicturesCache
    {
        public List<byte[]> Pictures;

        public KeyArtPicturesCache(byte[] picture)
        {
            Pictures = new List<byte[]>();
            Pictures.Add(picture);
        }

        public void AddCardPictureCache(byte[] picture)
        {
            Pictures.Add(picture);
        }
    }
}