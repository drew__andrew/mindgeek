﻿using System.Collections.Generic;

namespace MindGeek.Business.Models
{
    public class CardPicturesCache
    {
        public List<byte[]> Pictures;

        public CardPicturesCache(byte[] picture)
        {
            Pictures = new List<byte[]>();
            Pictures.Add(picture);
        }

        public void AddCardPictureCache(byte[] picture)
        {
            Pictures.Add(picture);
        }
    }
}