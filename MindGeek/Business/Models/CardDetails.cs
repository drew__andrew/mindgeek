﻿using System.Collections.Generic;

namespace MindGeek.Business.Models
{
    public class CardDetails 
    {
        public string Body { get; set; }

        public string Title { get; set; }

        public List<string> Cast { get; set; }

        public string Class { get; set; }

        public List<string> Directors { get; set; }

        public int Duration { get; set; }

        public string[] Genres { get; set; }

        public string Synopsis { get; set; }

        public string Year { get; set; }

        public List<Video> Videos { get; set; }

        public List<byte[]> KeyArtImages = new List<byte[]>();

        public List<byte[]> CardImages = new List<byte[]>();
    }
}