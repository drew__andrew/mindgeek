﻿namespace MindGeek.Business.Models
{
    public class Video
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}
