﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MindGeek.Business.Interfaces;
using MindGeek.Business.Models;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using MindGeek.DataAccess.Models.Dto;

namespace MindGeek.Business.Implementation
{
    public class FirstPageService : IFirstPageService
    {
        private readonly IReadRepository<BulkDataItem> _cardReadRepository;
        private readonly ILogger<FirstPageService> _logger;

        public FirstPageService(IReadRepository<BulkDataItem> CardReadRepository, ILogger<FirstPageService> Logger)
        {
            _cardReadRepository = CardReadRepository;
            _logger = Logger;
        }

        public List<Card> GetCardsForFirstPage()
        {
            var cards = _cardReadRepository.SearchFor<CardDto>(null);
            List<Card> webCards = new List<Card>();

            foreach (var item in cards)
            {
                webCards.Add(new Card
                {
                    Id = item.GetCardId(),
                    Pictures = item.Media.Where(x => x != null).ToList(),
                    Title = item.Title
                });
            }
            return webCards;
        }
    }
}
