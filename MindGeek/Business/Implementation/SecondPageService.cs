﻿using Microsoft.Extensions.Logging;
using MindGeek.Business.Interfaces;
using MindGeek.Business.Models;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using MindGeek.DataAccess.Models.Dto;
using System.Linq;

namespace MindGeek.Business.Implementation
{
    public class SecondPageService : ISecondPageService
    {
        private readonly IReadRepository<BulkDataItem> _cardDetailsReadRepository;
        private readonly ILogger<SecondPageService> _logger;

        public SecondPageService(IReadRepository<BulkDataItem> CardReadRepository, ILogger<SecondPageService> Logger)
        {
            _cardDetailsReadRepository = CardReadRepository;
            _logger = Logger;
        }

        public CardDetails GetCardDetails(string id)
        {
            var cardVideosDto = _cardDetailsReadRepository.SearchFor<CardVideoDto>(null, x => x.Id == id)
                .FirstOrDefault();

            var cardKeyArtImages = _cardDetailsReadRepository.SearchFor<CardKeyArtImagesDto>(null, x => x.Id == id)
                .FirstOrDefault();

            var cardImages = _cardDetailsReadRepository.SearchFor<CardImagesDto>(null, x => x.Id == id)
                .FirstOrDefault();

            var cardDetailsDto = _cardDetailsReadRepository.SearchFor<CardDetailsDto>(null, x => x.Id == id)
                .FirstOrDefault();

            CardDetails webCardDetails = new CardDetails
            {
                Body = cardDetailsDto?.Body,
                Class = cardDetailsDto?.Class,
                Duration = cardDetailsDto.Duration,
                Genres = cardDetailsDto?.Genres,
                Synopsis = cardDetailsDto?.Synopsis,
                Title = cardDetailsDto?.Title,
                Videos = cardVideosDto?.GetVideos.Select(x => new Video
                {
                    Title = x.Title,
                    Url = x.Url
                }).ToList(),
                KeyArtImages = cardKeyArtImages?.Media.Where(x => x != null).ToList(),
                CardImages = cardImages?.Media.Where(x => x != null).ToList(),
                Cast = cardDetailsDto?.Cast.Select(x => x.Name).ToList(),
                Directors = cardDetailsDto?.Directors.Select(x => x.Name).ToList(),
                Year = cardDetailsDto?.Year
            };

            return webCardDetails;
        }
    }
}