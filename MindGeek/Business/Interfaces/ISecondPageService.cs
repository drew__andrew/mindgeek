﻿using MindGeek.Business.Models;

namespace MindGeek.Business.Interfaces
{
    public interface ISecondPageService
    {
        CardDetails GetCardDetails(string id);
    }
}