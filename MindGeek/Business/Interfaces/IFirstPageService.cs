﻿using MindGeek.Business.Models;
using System.Collections.Generic;

namespace MindGeek.Business.Interfaces
{
    public interface IFirstPageService
    {
        List<Card> GetCardsForFirstPage();
    }
}
