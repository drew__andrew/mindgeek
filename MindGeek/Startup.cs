﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MindGeek.Business.Implementation;
using MindGeek.Business.Interfaces;
using MindGeek.Common;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.JsonLoader;
using MindGeek.DataAccess.Models;
using MindGeek.DataAccess.Repository;
using Serilog;


namespace MindGeek
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSingleton<ICacheDataHolder, CacheDataHolder>();
            services.AddScoped<IMediaDownloadHelper, MediaDownloadHelper>();
            services.AddScoped<IFirstPageService, FirstPageService>();
            services.AddScoped<ISecondPageService, SecondPageService>();
            services.AddScoped<IJsonLoader, JsonLoader>();
            services.AddScoped<IReadRepository<BulkDataItem>, DataReadRepository<BulkDataItem>>();



            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IJsonLoader jsonLoader, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            loggerFactory.AddSerilog();

            app.ConfigureCustomExceptionMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
