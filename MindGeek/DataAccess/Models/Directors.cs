﻿using Newtonsoft.Json;

namespace MindGeek.DataAccess.Models
{
    public class Directors
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
