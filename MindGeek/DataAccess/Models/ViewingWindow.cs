﻿using Newtonsoft.Json;

namespace MindGeek.DataAccess.Models
{
    public class ViewingWindow
    {
        [JsonProperty(PropertyName = "startDate")]
        public string StartDate { get; set; }

        [JsonProperty(PropertyName = "wayToWatch")]
        public string WayToWatch { get; set; }

        [JsonProperty(PropertyName = "endDate")]
        public string EndDate { get; set; }
    }
}
