﻿using Newtonsoft.Json;

namespace MindGeek.DataAccess.Models
{
    public class Alternatives
    {
        [JsonProperty(PropertyName = "quality")]
        public string Quality { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }
    }
}