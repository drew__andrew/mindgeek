﻿using Newtonsoft.Json;

namespace MindGeek.DataAccess.Models
{
    public class Cast
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}