﻿using MindGeek.Common;
using MindGeek.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MindGeek.Common.Helpers;

namespace MindGeek.DataAccess.Models.Dto
{
    public class CardDto : IBaseModel<BulkDataItem>, IDtoMediaCachable
    {
        public string Title;

        public string Id;

        public List<MetadataDto> MetadataDtos { get; set; }

        public List<byte[]> Media { get; set; } = new List<byte[]>();

        public MediaType MediaType { get; set; } = MediaType.CardPicture;


        public string GetCardId()
        {
            return Id;
        }

        public List<MetadataDto> GetMetadataDto()
        {
            return MetadataDtos;
        }

        public Expression<Func<BulkDataItem, IBaseModel<BulkDataItem>>> GetDefaultProjection()
        {
            return entity => new CardDto
            {
                Title = entity.Headline,
                Id = entity.Id,
                MetadataDtos = entity.CardImages.TransferMetadataFromDbToDtoObject()
            };
        }
    }
}