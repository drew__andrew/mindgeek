﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MindGeek.Common;
using MindGeek.DataAccess.Interfaces;

namespace MindGeek.DataAccess.Models.Dto
{
    public class CardDetailsDto : IBaseModel<BulkDataItem> 
    {
        public string Body { get; set; }

        public string Title { get; set; }

        public string Class { get; set; }

        public int Duration { get; set; }

        public string[] Genres { get; set; }

        public string Synopsis { get; set; }

        public string Year { get; set; }

        public List<CastDto> Cast { get; set; }

        public List<DirectorsDto> Directors { get; set; }

        public Expression<Func<BulkDataItem, IBaseModel<BulkDataItem>>> GetDefaultProjection()
        {
            return entity => new CardDetailsDto
            {
                Body = entity.Body,
                Title = entity.Headline,
                Class = entity.ClassValue,
                Duration = entity.Duration,
                Synopsis = entity.Synopsis,
                Year = entity.Year,
                Genres = entity.Genres,
                Cast = entity.Cast.TransferMetadataFromDbToDtoObject(),
                Directors = entity.Directors.TransferMetadataFromDbToDtoObject()
            };
        }
    }
}