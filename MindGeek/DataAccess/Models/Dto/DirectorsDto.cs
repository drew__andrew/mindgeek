﻿namespace MindGeek.DataAccess.Models.Dto
{
    public class DirectorsDto
    {
        public string Name { get; set; }
    }
}