﻿namespace MindGeek.DataAccess.Models.Dto
{
    public class VideoDto : BaseContentPathInfo
    {
        public string Title { get; set; }

        public string Type { get; set; }
    }
}