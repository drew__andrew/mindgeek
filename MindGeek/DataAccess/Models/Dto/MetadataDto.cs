﻿namespace MindGeek.DataAccess.Models.Dto
{
    public class MetadataDto : BaseContentPathInfo
    {
        public int W { get; set; }

        public int H { get; set; }
    }
}