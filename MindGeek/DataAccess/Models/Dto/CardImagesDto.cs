﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MindGeek.Common;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Interfaces;

namespace MindGeek.DataAccess.Models.Dto
{
    public class CardImagesDto :  IBaseModel<BulkDataItem>, IDtoMediaCachable
    {
        private string Id { get; set; }

        public List<byte[]> Media { get; set; } = new List<byte[]>();

        public List<MetadataDto> MetadataDtos { get; set; }

        public MediaType MediaType { get; set; } = MediaType.CardPicture;

        public string GetCardId()
        {
            return Id;
        }

        public Expression<Func<BulkDataItem, IBaseModel<BulkDataItem>>> GetDefaultProjection()
        {
            return entity => new CardImagesDto
            {
                Id = entity.Id,
                MetadataDtos = entity.CardImages.TransferMetadataFromDbToDtoObject()
            };
        }

        public List<MetadataDto> GetMetadataDto()
        {
            return MetadataDtos;
        }
    }
}