﻿using MindGeek.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MindGeek.DataAccess.Models.Dto
{
    public class CardVideoDto : IBaseModel<BulkDataItem>
    {
        public List<VideoDto> GetVideos { get; set; } = new List<VideoDto>();

        public Expression<Func<BulkDataItem, IBaseModel<BulkDataItem>>> GetDefaultProjection()
        {
            return entity => new CardVideoDto
            {
               GetVideos = entity.Videos.Select(x => new VideoDto
               {
                   Title = x.Title,
                   Type = x.Type,
                   Url = checkIfUrlHttps(x.Url)
               }).ToList()
            };
        }

        private string checkIfUrlHttps(string url)
        {
            if (!url.Contains("https"))
               return url.Replace("http", "https");

            return url;
        }
    }
}