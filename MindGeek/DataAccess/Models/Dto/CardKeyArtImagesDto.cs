﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MindGeek.Common;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Interfaces;

namespace MindGeek.DataAccess.Models.Dto
{
    public class CardKeyArtImagesDto : IBaseModel<BulkDataItem> , IDtoMediaCachable
    {
        public List<byte[]> Media { get; set; } = new List<byte[]>();

        public List<MetadataDto> MetadataDtos { get; set; }

        public MediaType MediaType { get; set; } = MediaType.KeyArtImages;

        public string Id;

        public string GetCardId()
        {
            return Id;
        }

        public List<MetadataDto> GetMetadataDto()
        {
            return MetadataDtos;
        }

        public Expression<Func<BulkDataItem, IBaseModel<BulkDataItem>>> GetDefaultProjection()
        {
            return entity => new CardKeyArtImagesDto 
            {
                Id = entity.Id,
                MetadataDtos = entity.KeyArtImages.TransferMetadataFromDbToDtoObject()
            };
        }
    }
}