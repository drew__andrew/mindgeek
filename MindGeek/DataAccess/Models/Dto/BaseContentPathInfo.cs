﻿namespace MindGeek.DataAccess.Models.Dto
{
    public abstract class BaseContentPathInfo
    {
        public string Url { get; set; }
    }
}