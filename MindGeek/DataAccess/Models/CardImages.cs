﻿using Newtonsoft.Json;

namespace MindGeek.DataAccess.Models
{
    public class CardImages
    {
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "h")]
        public int H { get; set; }

        [JsonProperty(PropertyName = "w")]
        public int W { get; set; }
    }
}