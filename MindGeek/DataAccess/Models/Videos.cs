﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MindGeek.DataAccess.Models
{
    public class Videos
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "alternatives")]
        public List<Alternatives> Alternatives { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }
    }
}