﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MindGeek.DataAccess.Models
{
    public class BulkDataItem
    {
        [JsonProperty(PropertyName = "body")]
        public string Body { get; set; }

        [JsonProperty(PropertyName = "cardImages")]
        public List<CardImages> CardImages { get; set; }

        [JsonProperty(PropertyName = "cast")]
        public List<Cast> Cast { get; set; }

        [JsonProperty(PropertyName = "cert")]
        public string Cert { get; set; }

        [JsonProperty(PropertyName = "class")]
        public string ClassValue { get; set; }

        [JsonProperty(PropertyName = "directors")]
        public List<Directors> Directors { get; set; }

        [JsonProperty(PropertyName = "duration")]
        public int Duration { get; set; }

        [JsonProperty(PropertyName = "genres")]
        public string[] Genres { get; set; }

        [JsonProperty(PropertyName = "headline")]
        public string Headline { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "keyArtImages")]
        public List<KeyArtImages> KeyArtImages { get; set; }

        [JsonProperty(PropertyName = "lastUpdated")]
        public string LastUpdated { get; set; }

        [JsonProperty(PropertyName = "quote")]
        public string Quote { get; set; }

        [JsonProperty(PropertyName = "rating")]
        public string Rating { get; set; }

        [JsonProperty(PropertyName = "reviewAuthor")]
        public string ReviewAuthor { get; set; }

        [JsonProperty(PropertyName = "skyGoId")]
        public string SkyGoId { get; set; }

        [JsonProperty(PropertyName = "skyGoUrl")]
        public string SkyGoUrl { get; set; }

        [JsonProperty(PropertyName = "sum")]
        public string Sum { get; set; }

        [JsonProperty(PropertyName = "synopsis")]
        public string Synopsis { get; set; }

        [JsonProperty(PropertyName = "videos")]
        public List<Videos> Videos { get; set; }

        [JsonProperty(PropertyName = "viewingWindow")]
        public ViewingWindow ViewingWindow { get; set; }

        [JsonProperty(PropertyName = "year")]
        public string Year { get; set; }

    }
}