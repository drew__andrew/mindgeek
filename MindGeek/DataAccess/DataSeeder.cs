﻿using System.Collections.Generic;
using System.Linq;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;

namespace MindGeek.DataAccess
{
    public static class DataSeeder
    {
        static DataSeeder() { }

        private static List<BulkDataItem> MemoryData;

        public static void FetchData(IJsonLoader jsonLoader)
        {
            MemoryData = jsonLoader.LoadJsonFromUrl().Result;
        }

        public static IQueryable<BulkDataItem> GetMemoryData()
        {
            return MemoryData.AsQueryable();
        }
    }
}