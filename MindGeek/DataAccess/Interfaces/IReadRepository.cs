﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MindGeek.DataAccess.Models;

namespace MindGeek.DataAccess.Interfaces
{
    public interface IReadRepository<T>
    {
        IEnumerable<TModel> SearchFor<TModel>(
            Expression<Func<BulkDataItem, TModel>> projection,
            Expression<Func<BulkDataItem, bool>> predicate = null)
            where TModel : IBaseModel<BulkDataItem>, new();
    }
}