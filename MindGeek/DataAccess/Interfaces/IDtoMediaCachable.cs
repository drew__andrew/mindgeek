﻿using System.Collections.Generic;
using MindGeek.DataAccess.Models.Dto;
using MediaType = MindGeek.Common.Helpers.MediaType;

namespace MindGeek.DataAccess.Interfaces
{
    public interface IDtoMediaCachable
    {
        List<byte[]> Media { get; set; }

        List<MetadataDto> MetadataDtos { get; set; }

        MediaType MediaType { get; set; }

        string GetCardId();

        List<MetadataDto> GetMetadataDto();

    }
}
