﻿using MindGeek.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MindGeek.DataAccess.Interfaces
{
    public interface IJsonLoader
    {
        Task<List<BulkDataItem>> LoadJsonFromUrl();
    }
}