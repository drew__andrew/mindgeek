﻿using System;
using System.Linq.Expressions;

namespace MindGeek.DataAccess.Interfaces
{
    public interface IBaseModel<TEntity>
    {
        Expression<Func<TEntity, IBaseModel<TEntity>>> GetDefaultProjection();
    }
}
