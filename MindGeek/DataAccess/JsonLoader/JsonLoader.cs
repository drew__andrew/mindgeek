﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MindGeek.Common.Exceptions;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using Newtonsoft.Json;

namespace MindGeek.DataAccess.JsonLoader
{
    public class JsonLoader : IJsonLoader
    {
        private readonly HttpClient client = new HttpClient();

        private readonly Uri uri = new Uri("https://mgtechtest.blob.core.windows.net/files/showcase.json");

        private HttpResponseMessage response;

        public async Task<List<BulkDataItem>> LoadJsonFromUrl()
        {
            response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var byteArray = response.Content.ReadAsByteArrayAsync().Result;
                Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                var result = iso.GetString(byteArray);
                List<BulkDataItem> json = JsonConvert.DeserializeObject<List<BulkDataItem>>(result);
                return json;
            }
            else
            {
                throw new JsonDownloadFailureException(uri.AbsoluteUri);
            }
        }
    }
}