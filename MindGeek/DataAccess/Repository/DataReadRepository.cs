﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MindGeek.Common;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;

namespace MindGeek.DataAccess.Repository
{
    public class DataReadRepository<T> : IReadRepository<T>
    {
        private IMediaDownloadHelper _mediaDownloadHelper;

        public DataReadRepository(IMediaDownloadHelper mediaDownloadHelper)
        {
            _mediaDownloadHelper = mediaDownloadHelper;
        }

        /// <summary>
        /// Method for parsing the static data fetched at the start of the application
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="projection">The object transformation</param>
        /// <param name="predicate">For future filtering</param>
        /// <returns></returns>
        public IEnumerable<TModel> SearchFor<TModel>(Expression<Func<BulkDataItem, TModel>> projection, Expression<Func<BulkDataItem, bool>> predicate = null) where TModel : IBaseModel<BulkDataItem>, new()
        {
            Expression<Func<BulkDataItem, TModel>> projectionExpression =
                projection ?? ProjectionHelper<BulkDataItem, TModel>.GetDefaultProjection();

            if (predicate == null)
                return DataSeeder.GetMemoryData().Select(projectionExpression).ToList().InterceptMediaForCaching(_mediaDownloadHelper);
            return DataSeeder.GetMemoryData().Where(predicate).Select(projectionExpression).ToList()
                .InterceptMediaForCaching(_mediaDownloadHelper);
        }
    }
}
