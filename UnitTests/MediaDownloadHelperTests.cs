﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using MindGeek.Business.Models;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models.Dto;
using Moq;
using Xunit;

namespace UnitTests
{
    public class MediaDownloadHelperTests
    {
        private Mock<ICacheDataHolder> _dataHolderMock;
        private Mock<ILogger<MediaDownloadHelper>> _loggerMock;
        private MediaDownloadHelper mediaDownloadHelper;

        public MediaDownloadHelperTests()
        {
           _loggerMock = new Mock<ILogger<MediaDownloadHelper>>();
        }

        [Fact]
        public void MediaDownloadHelperTests__TryDownloadMedia_CheckIfMediaIsTakenFromCache()
        {
            IDtoMediaCachable cachableMedia = new CardDto();
            _dataHolderMock = new Mock<ICacheDataHolder>();

            _dataHolderMock.Setup(x => x.CheckIfMediaShouldDownload(It.IsAny<IDtoMediaCachable>())).Returns(false);
            _dataHolderMock.Setup(x => x.GetCardPicturesCache(It.IsAny<string>())).Returns(new CardPicturesCache(new byte[2]{3,5}));

            mediaDownloadHelper = new MediaDownloadHelper(_dataHolderMock.Object, _loggerMock.Object);
            mediaDownloadHelper.TryDownloadMedia(cachableMedia);

            Assert.Equal(cachableMedia.Media, new List<byte[]>{new byte[2]{3,5}});

        }
    }
}
