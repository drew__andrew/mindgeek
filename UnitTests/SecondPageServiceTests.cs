﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using Moq;
using MindGeek.Business.Implementation;
using Microsoft.Extensions.Logging;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Models.Dto;
using Xunit;
using MindGeek.Business.Models;
using Newtonsoft.Json;

namespace UnitTests
{
    public class SecondPageServiceTests
    {
        private Mock<IReadRepository<BulkDataItem>> _cardDetailsReadRepositoryMock;
        private SecondPageService serviceToTest;
        private Mock<ILogger<SecondPageService>> _loggerMock;

        public SecondPageServiceTests()
        {
            _cardDetailsReadRepositoryMock = new Mock<IReadRepository<BulkDataItem>>();
            _loggerMock = new Mock<ILogger<SecondPageService>>();
            serviceToTest = new SecondPageService(_cardDetailsReadRepositoryMock.Object, _loggerMock.Object);
        }

        [Fact]
        public void FirstPageService_GetCardDetails_ShouldReturnCorrectResult()
        {
            CardVideoDto video1 = new CardVideoDto
            {
                GetVideos = new List<VideoDto>
                {
                    new VideoDto
                    {
                        Title = "test1",
                        Type = "type1",
                        Url = "https://test"
                    }
                }
            };

            CardKeyArtImagesDto cardKeyArtImages = new CardKeyArtImagesDto
            {
                Media = new List<byte[]>
                {
                    new byte[2] {1, 2}
                },
                MediaType = MediaType.KeyArtImages,
                MetadataDtos = new List<MetadataDto>
                {
                    new MetadataDto
                    {
                        H = 5,
                        W = 5,
                        Url = "https://test2"
                    }
                }
            };

            CardImagesDto cardImagesDto = new CardImagesDto
            {
                Media = new List<byte[]>
                {
                    new byte[2] {1, 2}
                },
                MediaType = MediaType.CardPicture,
                MetadataDtos = new List<MetadataDto>
                {
                    new MetadataDto
                    {
                        H = 5,
                        W = 5,
                        Url = "https://test2"
                    }
                }
            };

            CardDetailsDto cardDetailsDto = new CardDetailsDto
            {
                Body = "bodyTest",
                Cast = new List<CastDto>
                {
                    new CastDto
                    {
                        Name = "test"
                    }
                },
                Class = "Movie",
                Directors = new List<DirectorsDto>
                {
                    new DirectorsDto
                    {
                        Name = "test23232"
                    }
                },
                Duration = 55554,
                Genres = new []{"one","two"},
                Synopsis = "synopsis test",
                Title = "title test",
                Year = "2019"
            };

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardVideoDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardVideoDto>{video1});

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardKeyArtImagesDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardKeyArtImagesDto> { cardKeyArtImages });

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardImagesDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardImagesDto> { cardImagesDto });

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardDetailsDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardDetailsDto> { cardDetailsDto });

            var expectedResponse =
                JsonConvert.SerializeObject(PrepareCardDetails(video1, cardKeyArtImages, cardImagesDto,
                    cardDetailsDto));

            var response = JsonConvert.SerializeObject(serviceToTest.GetCardDetails(It.IsAny<string>()));


            Assert.Equal(expectedResponse, response);

        }

        [Fact]
        public void FirstPageService_GetCardDetails_ShouldReturnCorrectResult_IfOneEntityIsNull()
        {

            CardKeyArtImagesDto cardKeyArtImages = new CardKeyArtImagesDto
            {
                Media = new List<byte[]>
                {
                    new byte[2] {1, 2}
                },
                MediaType = MediaType.KeyArtImages,
                MetadataDtos = new List<MetadataDto>
                {
                    new MetadataDto
                    {
                        H = 5,
                        W = 5,
                        Url = "https://test2"
                    }
                }
            };

            CardImagesDto cardImagesDto = new CardImagesDto
            {
                Media = new List<byte[]>
                {
                    new byte[2] {1, 2}
                },
                MediaType = MediaType.CardPicture,
                MetadataDtos = new List<MetadataDto>
                {
                    new MetadataDto
                    {
                        H = 5,
                        W = 5,
                        Url = "https://test2"
                    }
                }
            };

            CardDetailsDto cardDetailsDto = new CardDetailsDto
            {
                Body = "bodyTest",
                Cast = new List<CastDto>
                {
                    new CastDto
                    {
                        Name = "test"
                    }
                },
                Class = "Movie",
                Directors = new List<DirectorsDto>
                {
                    new DirectorsDto
                    {
                        Name = "test23232"
                    }
                },
                Duration = 55554,
                Genres = new[] { "one", "two" },
                Synopsis = "synopsis test",
                Title = "title test",
                Year = "2019"
            };

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardVideoDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardVideoDto>());

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardKeyArtImagesDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardKeyArtImagesDto> { cardKeyArtImages });

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardImagesDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardImagesDto> { cardImagesDto });

            _cardDetailsReadRepositoryMock.Setup(x =>
                x.SearchFor(It.IsAny<Expression<Func<BulkDataItem, CardDetailsDto>>>(),
                    It.IsAny<Expression<Func<BulkDataItem, bool>>>())).Returns(new List<CardDetailsDto> { cardDetailsDto });

            var expectedResponse =
                JsonConvert.SerializeObject(PrepareCardDetails(null, cardKeyArtImages, cardImagesDto,
                    cardDetailsDto));

            var response = JsonConvert.SerializeObject(serviceToTest.GetCardDetails(It.IsAny<string>()));


            Assert.Equal(expectedResponse, response);

        }


        private CardDetails PrepareCardDetails(CardVideoDto video, CardKeyArtImagesDto keyArt, CardImagesDto cardImages,
            CardDetailsDto cardDetails)
        {
            return new CardDetails
            {
                Body = cardDetails.Body,
                CardImages = cardImages.Media,
                Cast = cardDetails.Cast.Select(x => x.Name).ToList(),
                Directors = cardDetails.Directors.Select(x => x.Name).ToList(),
                Class = cardDetails.Class,
                Duration = cardDetails.Duration,
                Genres = cardDetails.Genres,
                KeyArtImages = keyArt.Media,
                Synopsis = cardDetails.Synopsis,
                Title = cardDetails.Title,
                Videos = video?.GetVideos.Select(x => new Video
                {
                    Title = x.Title,
                    Url = x.Url
                }).ToList(),
                Year = "2019"
            };
        }
    }
}
