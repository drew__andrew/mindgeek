﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MindGeek.DataAccess;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using Moq;
using Xunit;

namespace UnitTests
{
    public class DataSeederTests
    {
        private Mock<IJsonLoader> _mockJsonLoader;

        public DataSeederTests()
        {
            _mockJsonLoader = new Mock<IJsonLoader>();
        }

        [Fact]
        public void DataSeeder_FetchData_ShouldLoadDataToMemory()
        {
            List<BulkDataItem> json = new List<BulkDataItem>();
            _mockJsonLoader.Setup(x => x.LoadJsonFromUrl()).Returns(Task.FromResult(json));
            DataSeeder.FetchData(_mockJsonLoader.Object);
            Assert.Equal(DataSeeder.GetMemoryData(), json.AsQueryable());
        }
    }
}