﻿using System.Linq;
using MindGeek.Common.Helpers;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models.Dto;
using Moq;
using Xunit;

namespace UnitTests
{
    public class CacheDataHolderTests
    {
        private CacheDataHolder _cacheDataHolder;

        private IDtoMediaCachable _cachableMedia;

        public CacheDataHolderTests()
        {
            _cacheDataHolder = new CacheDataHolder();
        }

        [Fact]
        public void CacheDataHolderTests_TryInsertDataToDictionary_ShouldInsert1ValueIfCardIsNotPresentInKey()
        {
            string cardId = "abc";
            byte[] data = new byte[] {1, 23, 4};
            _cacheDataHolder.TryInsertDataToDictionary(cardId, data , MediaType.CardPicture);

            var response = _cacheDataHolder.GetCardPicturesCache(cardId).Pictures;
            Assert.Single(response);
        }

        [Fact]
        public void CacheDataHolderTests_GetCardPicturesCache_ShouldReturn2ValuesIfCardIsPresentInKey()
        {
            string cardId = "abc";
            byte[] data = new byte[] { 1, 23, 4 };
            byte[] data2 = new byte[] { 1, 23, 29 };

            _cacheDataHolder.TryInsertDataToDictionary(cardId, data, MediaType.CardPicture);
            _cacheDataHolder.TryInsertDataToDictionary(cardId, data2, MediaType.CardPicture);

            var response = _cacheDataHolder.GetCardPicturesCache(cardId).Pictures;

            Assert.Equal(2, response.Count);
            Assert.NotEqual(response.FirstOrDefault(), response.LastOrDefault());
        }

        [Fact]
        public void CacheDataHolderTests_CheckIfCardPicturesShouldDownload_ShouldReturnTrueIfDataIsNotPresent()
        {
            _cachableMedia = new CardDto
            {
                Id = "abc"
            };

            var response = _cacheDataHolder.CheckIfMediaShouldDownload(_cachableMedia);
            Assert.True(response);
        }

        [Fact]
        public void CacheDataHolderTests_CheckIfKeyArtPicturesShouldDownload_ShouldReturnFalseIfDataIsPresent()
        {
            string cardId = "abc";
            byte[] data = new byte[] {1, 23, 4};
            _cacheDataHolder.TryInsertDataToDictionary(cardId, data, MediaType.KeyArtImages);

            _cachableMedia = new CardKeyArtImagesDto
            {
                Id = cardId,
                MediaType = MediaType.KeyArtImages
            };

            var response = _cacheDataHolder.CheckIfMediaShouldDownload(_cachableMedia);
            Assert.False(response);
        }
    }
}
