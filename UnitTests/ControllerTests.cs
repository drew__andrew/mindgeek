﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MindGeek.Business.Interfaces;
using MindGeek.Business.Models;
using MindGeek.Web.Controllers;
using Moq;
using Xunit;

namespace UnitTests
{
    public class ControllerTests
    {
        private Mock<IFirstPageService> _firstPageServiceMock;
        private Mock<ISecondPageService> _secondPageServiceMock;
        private Mock<ILogger<HomeController>> _homeControllerLoggerMock;
        private HomeController _homeController;

        public ControllerTests()
        {
            _firstPageServiceMock = new Mock<IFirstPageService>();
            _secondPageServiceMock = new Mock<ISecondPageService>();
            _homeControllerLoggerMock = new Mock<ILogger<HomeController>>();
            _homeController = new HomeController(_firstPageServiceMock.Object, _secondPageServiceMock.Object, _homeControllerLoggerMock.Object);
        }

        [Fact]
        public void HomeController_Index_ShouldReturnCorrectView()
        {
            _firstPageServiceMock.Setup(x => x.GetCardsForFirstPage()).Returns(new List<Card>
            {
                new Card
                {
                    Id = "abc",
                    Title = "testTitle",
                    Pictures = new List<byte[]>
                    {
                        new byte[2] {1, 2}
                    }
                }
            });

            var result = _homeController.Index();

            var viewResult = Assert.IsType<ViewResult>(result);

            var model = Assert.IsAssignableFrom<List<Card>>(
                viewResult.ViewData.Model);

            Assert.Single(model);
        }

        [Fact]
        public void HomeController_CardDetails_ShouldReturnCorrectView()
        {
            _secondPageServiceMock.Setup(x => x.GetCardDetails(It.IsAny<string>())).Returns(new CardDetails());

            var result = _homeController.CardDetails(It.IsAny<string>());

            var viewResult = Assert.IsType<ViewResult>(result);

            var model = Assert.IsAssignableFrom<CardDetails>(
                viewResult.ViewData.Model);

            Assert.IsType<CardDetails>(model);
        }

    }
}
