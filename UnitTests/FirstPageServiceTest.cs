﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MindGeek.Business.Implementation;
using MindGeek.Business.Models;
using MindGeek.DataAccess.Interfaces;
using MindGeek.DataAccess.Models;
using MindGeek.DataAccess.Models.Dto;
using Moq;
using Xunit;

namespace UnitTests
{
    public class FirstPageServiceTest
    {
        private Mock<IReadRepository<BulkDataItem>> _cardReadRepositoryMock;
        private FirstPageService serviceToTest;
        private Mock<ILogger<FirstPageService>> _loggerMock;

        public FirstPageServiceTest()
        {
            _cardReadRepositoryMock = new Mock<IReadRepository<BulkDataItem>>();
            _loggerMock = new Mock<ILogger<FirstPageService>>();
            serviceToTest = new FirstPageService(_cardReadRepositoryMock.Object, _loggerMock.Object);
        }

        [Fact]
        public void FirstPageService_GetCardsForFirstPage_ShouldReturnCorrectResult_1()
        {
            List<CardDto> mockedCardsResponse = new List<CardDto>
            {
                new CardDto
                {
                    Id = "abc",
                    Media = new List<byte[]>
                    {
                        new byte[]{2,4}
                    },
                    MetadataDtos = new List<MetadataDto>
                    {
                        new MetadataDto
                        {
                            H = 5,
                            W = 10,
                            Url = "http://test"
                        }
                    },
                    Title = "Test"
                },
                new CardDto
                {
                    Id = "abc",
                    Media = new List<byte[]>
                    {
                        new byte[]{2,4}
                    },
                    MetadataDtos = new List<MetadataDto>
                    {
                        new MetadataDto
                        {
                            H = 55,
                            W = 10,
                            Url = "http://test"
                        }
                    },
                    Title = "Test2"
                }
            };

            _cardReadRepositoryMock.Setup(x => x.SearchFor<CardDto>(null, null)).Returns(mockedCardsResponse);
            var response = serviceToTest.GetCardsForFirstPage();

            Assert.IsType<List<Card>>(response);
            Assert.Equal("Test", response.FirstOrDefault().Title);
            Assert.Equal(2, response.Count);
        }

        [Fact]
        public void FirstPageService_GetCardsForFirstPage_Should_Return_Only_NotNullImages()
        {
            List<CardDto> mockedCardsResponse = new List<CardDto>
            {
                new CardDto
                {
                    Id = "abc",
                    Media = new List<byte[]>
                    {
                        new byte[]{2,4},
                        null
                    },
                    MetadataDtos = new List<MetadataDto>
                    {
                        new MetadataDto
                        {
                            H = 5,
                            W = 10,
                            Url = "http://test"
                        }
                    },
                    Title = "Test"
                }
            };

            _cardReadRepositoryMock.Setup(x => x.SearchFor<CardDto>(null, null)).Returns(mockedCardsResponse);
            var response = serviceToTest.GetCardsForFirstPage();

            Assert.Single(response.FirstOrDefault().Pictures);
        }
    }
}