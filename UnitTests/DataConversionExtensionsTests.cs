﻿using MindGeek.DataAccess.Models;
using System.Collections.Generic;
using MindGeek.Common;
using MindGeek.DataAccess.Models.Dto;
using Xunit;

namespace UnitTests
{
    public class DataConversionExtensionsTests
    {

        [Fact]
        public void DataConversionExtension_TransferMetadataFromDbToDtoObject_ShouldReturnCorrectResponse_1()
        {
            List<CardImages> cardImage = new List<CardImages>
            {
                new CardImages
                {
                    H = 5,
                    W = 5,
                    Url = "https://local"
                }
            };

            var response = cardImage.TransferMetadataFromDbToDtoObject();

            Assert.IsType<List<MetadataDto>>(response);
            Assert.Single(response);
        }


        [Fact]
        public void DataConversionExtension_TransferMetadataFromDbToDtoObject_ShouldReturnCorrectResponse_2()
        {
            List<KeyArtImages> keyArtImages = new List<KeyArtImages>
            {
                new KeyArtImages
                {
                    H = 5,
                    W = 5,
                    Url = "https://local"
                }
            };

            var response = keyArtImages.TransferMetadataFromDbToDtoObject();

            Assert.IsType<List<MetadataDto>>(response);
            Assert.Single(response);
        }
    }
}
